package edu.bu.ec504.hw3.p2;

import edu.bu.ec504.hw3.p2.Canvas.CanvasPoint;
import java.util.ArrayList;

public class Main {

    /**
     * Run a simple test of the MyCanvas class.
     * @param baseX The x dimension of the underlying canvas.
     * @param baseY The y dimension of the underlying canvas.
     * @return A CoverResult, which contains the canvas produced and its proposed covering.
     */
    static CoverResult testCovering(int baseX, int baseY) {
        Canvas testCanvas = new MyCanvas(baseX,baseY);
        for (int ii=0; ii<Math.min(baseX,baseY); ii++)
            testCanvas.addPoint(new CanvasPoint(ii,ii));

        // show results
        ArrayList<CanvasPoint> testCovering = testCanvas.generateCovering();
        System.out.println("Produced covering:");
        System.out.println(testCovering);
        if (testCanvas.checkCovering(testCovering)) {
            System.out.println(" ... covering is valid!");
        }
        else {
            throw new RuntimeException("Covering is invalid!");
        }

        // return results
        return new CoverResult(testCanvas, testCovering);
    }

    public static void main(String[] args) {
        CoverResult result = testCovering(100,100);
    }

    static public class CoverResult {

        public CoverResult(Canvas theCanvas, ArrayList<CanvasPoint> theCenters) {
            canvas = theCanvas;
            centers = theCenters;
        }

        final public Canvas canvas;
        final public ArrayList<CanvasPoint> centers;
    }
}
